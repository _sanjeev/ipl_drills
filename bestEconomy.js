const delivery = require('./delivery.json');
const match = require('./matches.json');

const bestEconomy = ((match, delivery) => {

    const data = delivery.filter ((val) => {
        return val.is_super_over !== '0';
    })

    const out = data.reduce ((acc, val) => {
        if (!acc[val.bowler]) {
            acc[val.bowler] = [];
            acc[val.bowler][0] = 1;
            acc[val.bowler][1] = Number(val.total_runs);
        }else {
            acc[val.bowler][0]++;
            acc[val.bowler][1] += Number (val.total_runs);
        }
        return acc;
    }, {})

    const output = Object.entries (out).reduce ((acc, val) => {
        let over = Math.floor (val[1][0] / 6);
        let ball = (val[1][0] % 6) * 0.1;
        let totalOver = over + ball;
        let economy = (val[1][1] / totalOver).toFixed (2);
        acc[val[0]] = economy;
        return acc;
    }, {})
    console.log(output);
})

bestEconomy (match, delivery);