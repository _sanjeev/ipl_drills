const matches = require ('./matches.json');
const delivery = require ('./delivery.json');

const problem2 = ((matches) => {
    const output = matches.reduce ((acc, val) => {
        if (!acc[val.winner]) {
            acc[val.winner] = {};
            acc[val.winner][val.season] = 1;
        }else {
            if (!acc[val.winner][val.season]) {
                acc[val.winner][val.season] = 1;
            }else {
                acc[val.winner][val.season]++;
            }
        }
        return acc;
    }, {})
    console.log(output);
})

problem2 (matches);