const delivery = require('./delivery.json');
const match = require('./matches.json');


const getData = ((match, delivery) => {
    const data = delivery.map ((key) => {
        const out = match.find ((val) => {
            return val.id === key.match_id;
        })
        key['season'] = out.season;
        return key;
    })

    const out = data.reduce ((acc, val) => {
        if (!acc[val.batsman]) {
            acc[val.batsman] = {};
            acc[val.batsman][val.season] = [];
            acc[val.batsman][val.season][0] = 1;
            acc[val.batsman][val.season][1] = Number(val.total_runs); 
        }else {
            if (!acc[val.batsman][val.season]) {
                acc[val.batsman][val.season] = [];
                acc[val.batsman][val.season][0] = 1;
                acc[val.batsman][val.season][1] = Number (val.total_runs);
            }else {
                acc[val.batsman][val.season][0]++;
                acc[val.batsman][val.season][1] += Number (val.total_runs);
            }
        }
        return acc;
    }, {})
    
    const output = Object.entries (out).reduce ((acc, val) => {
        acc[val[0]] = Object.entries (val[1]).reduce ((accu, value) => {
            
            // let totalRuns = value[1][1];
            // let totalOver = (Math.floor(value[1][0] / 6) + ((value[1][0] % 6) * 0.1));
            let economy = ((value[1][1] / value[1][0]) * 100).toFixed(2); 
            accu[value[0]] = economy;
            return accu;
            
        }, {})
        return acc;
    },{})
    console.log(output);
})

getData (match, delivery);