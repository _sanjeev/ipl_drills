const delivery = require('./delivery.json');
const match = require('./matches.json');

const playerOfMatch = ((delivery, match) => {

    const out = match.reduce((acc, val) => {

        if (!acc[val.player_of_match]) {
            acc[val.player_of_match] = {};
            acc[val.player_of_match][val.season] = 1;
        } else {
            if (!acc[val.player_of_match][val.season]) {
                acc[val.player_of_match][val.season] = 1;
            } else {
                acc[val.player_of_match][val.season]++;
            }
        }
        return acc;

    }, {})


    console.log(out);

    const output = Object.entries (out).reduce ((acc, val) => {
        acc[val[0]] = (Object.entries(val[1]).map ((key) => {
            key.sort ((a, b) => {
                return a[1] - b[1];
            })
            return key;
        }))
        return acc;
    }, {})


    // console.log(output);
    
})

playerOfMatch(delivery, match);