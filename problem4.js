const matches = require ('./matches.json');
const delivery = require ('./delivery.json');

const toptenBowler = ((matches, delivery) => {

    const data2015 = matches.filter ((key) => {
        return key.season === '2015';
    })

    const newData = delivery.filter ((key) => {
        return key.match_id >= data2015[0].id && key.match_id <= data2015[data2015.length - 1].id;
    })

    const data = newData.reduce ((acc, val) => {
        if (!acc[val.bowler]) {
            acc[val.bowler] = [];
            acc[val.bowler][0] = 1;
            acc[val.bowler][1] = Number(val.total_runs);
        }else {
            acc[val.bowler][0] += 1;
            acc[val.bowler][1] = Number (val.total_runs);
        }
        return acc;
    }, {})
    
    const outptu = Object.fromEntries(Object.entries (data).map ((key) => {
        let over = Math.floor(key[1][0] / 6) + ((key[1][0] % 6) * 0.1);
        let economy = (key[1][1] / over).toFixed(2);
        key[1] = economy;
        return key;
    }).sort ((a, b) => {
        return b[1] - a[1];
    }).slice (0, 10));
    
    console.log(outptu);

})

toptenBowler (matches, delivery);