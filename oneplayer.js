const delivery = require('./delivery.json');
const match = require('./matches.json');


const findHighest = (() => {
    const data = delivery.reduce ((acc, val) => {
        if (val.dismissal_kind != 'run out' && val.player_dismissed != '') {
            if (!acc[val.batsman]) {
                acc[val.batsman] = {};
                acc[val.batsman][val.bowler] = 1;
            }else {
                if (!acc[val.batsman][val.bowler]) {
                    acc[val.batsman][val.bowler] = 1;
                }else {
                    acc[val.batsman][val.bowler]++;
                }
            }
        } 
        return acc;       
    }, {})
    console.log(data);
})

findHighest();