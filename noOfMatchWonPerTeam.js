const delivery = require ('./delivery.json');
const match = require ('./matches.json');

const getData = ((match) => {
    const result = match.reduce ((acc, val) => {
        if (!acc[val.winner]) {
            acc[val.winner] = {};
            acc[val.winner][val.season] = 1;
        }else {
            if (!acc[val.winner][val.season]) {
                acc[val.winner][val.season] = 1;
            }else {
                acc[val.winner][val.season]++;
            }
        }
        return acc;
    }, {})
    console.log(result);
})

getData (match);