const matches = require ('./matches.json');
const delivery = require ('./delivery.json');

const problem3 = ((matches, delivery) => {
    const res = delivery.map ((key) => {
        const res = matches.find ((val) => {
            return val.id === key.match_id;
        })
        key['season'] = res.season;
        return key;
    })
    const result = res.filter ((key) => {
        if (key.season === '2016' && key.extra_runs !== '0') {
            return true;
        }
    });
    const output = result.reduce ((acc, val) => {
        if (!acc[val.batting_team]) {
            acc[val.batting_team] = Number(val.extra_runs);
        }else {
            acc[val.batting_team] += Number (val.extra_runs);
        }
        return acc;
    }, {})
    console.log(output);
})

problem3 (matches, delivery);