const matches = require ('./matches.json');
const delivery = require ('./delivery.json');

const wonToss = ((matches, delivery) => {

    const data = matches.reduce ((acc, val) => {

        if (val.winner === val.toss_winner) {
            if (!acc[val.winner]) {
                acc[val.winner] = 1;
            }else {
                acc[val.winner]++;
            }
        }
        return acc;

    }, {})

    console.log(data);

})

wonToss (matches, delivery);