const delivery = require('./delivery.json');
const match = require('./matches.json');


const strikeRate = ((match, delivery) => {

    const data = delivery.map((key) => {
        const out = match.find((val) => {
            return val.id === key.match_id;
        })
        key['season'] = out.season;
        return key;
    })


    const strikeRate = data.reduce((acc, val) => {
        if (!acc[val.batsman]) {
            acc[val.batsman] = {};
            acc[val.batsman][val.season] = [];
            acc[val.batsman][val.season][0] = 1;
            acc[val.batsman][val.season][1] = Number(val.total_runs);
        } else {
            if (!acc[val.batsman][val.season]) {
                acc[val.batsman][val.season] = [];
                acc[val.batsman][val.season][0] = 1;
                acc[val.batsman][val.season][1] = Number(val.total_runs);
            } else {
                acc[val.batsman][val.season][0]++;
                acc[val.batsman][val.season][1] += Number(val.total_runs);
            }
        }
        return acc;
    }, {})


    const out = Object.entries (strikeRate).reduce ((acc, val) => {
        acc[val[0]] = Object.entries (val[1]).reduce ((accu, value) => {
            accu[value[0]] = ((value[1][1] / value[1][0]) * 100).toFixed(2);
            return accu;
        }, {})
        return acc;
    }, {})

    console.log(out);

})

strikeRate(match, delivery);