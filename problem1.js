const matches = require ('./matches.json');
const delivery = require ('./delivery.json');

const problem1 = ((matches) => {
    const output = matches.reduce ((acc, val) => {
        if (!acc[val.season]) {
            acc[val.season] = 1;
        }else {
            acc[val.season]++;
        }
        return acc;
    }, {})
    console.log(output);
})

problem1 (matches);

// {
//     2008: countOfmatchesPlayedInThisYear,
//     2009: countOfmatchesPlayedInThisYear
// }